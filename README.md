# Prueba Ixpandit - Pokeapp

## Installation

### Step 1: Install requirements

```bash
  pip install -r requirements.txt
```

### Step 2: Migrate database
```bash
  python manage.py migrate pokeapp
```

### Step 3: Load cache (Optional)

Load the cache of the image url, if the fixture is not loaded the search could take more time

```bash
  python manage.py loaddata fixtures/image_cache.json
```

## Run Application

```bash
  python manage.py runserver
```

## Run Test

```bash
  python manage.py test
```
