from django.db import models


class PokemonImageCache(models.Model):
    pokemon_id = models.CharField(max_length=255)
    url_image = models.CharField(max_length=255)
