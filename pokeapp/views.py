# Django imports
from django.shortcuts import render

# Local improts
from pokeapp import forms
from pokeapp.services import search_pokemons


def home(request):
    pokemons = []
    if request.method == 'POST':
        form = forms.PokemonForm(request.POST)
        if form.is_valid():
            name = form.data['pokemon_name']
            pokemons = search_pokemons(name=name)
    else:
        form = forms.PokemonForm(request.POST)

    return render(request, 'home.html', {'form': form, 'pokemons': pokemons})
