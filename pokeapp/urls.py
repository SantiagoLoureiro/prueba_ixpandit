# Djagno imports
from django.urls import path

# Local imports
from pokeapp import views

urlpatterns = [
    path(r'', views.home, name='home'),
]
