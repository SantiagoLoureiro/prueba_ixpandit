from django import forms


class PokemonForm(forms.Form):
    pokemon_name = forms.CharField(label='Pokemon-name',
                                   max_length=100,
                                   widget=forms.TextInput(
                                       attrs=
                                          {
                                              'placeholder': 'Ingrese el nombre a buscar',
                                              'class': "field"
                                          }
                                    )
                                   )
