# Django imports
from django.core.exceptions import ObjectDoesNotExist

# Local imports
from pokeapp.utils import Connector
from pokeapp.utils import get_id_from_pokemon_dict
from pokeapp.models import PokemonImageCache

# Python imports
import os
from typing import Dict


class PokemonApiController(object):
    pokeapi_version = os.getenv(
        'POKEAPI_VERSION',
        'v2'
    )
    base_url = os.getenv(
        'POKEMON_API_URL',
        f'https://pokeapi.co/api/{pokeapi_version}/'
    )

    def __init__(self):
        self.connector = Connector(self.base_url)

    def get_all_pokemons(self) -> Dict:
        data = self.connector.get(
            resource='pokemon/?limit=3000'
        )
        return data['results']

    def get_pokemon_image_url(
        self,
        pokemon_dict: Dict
    ) -> str or None:

        pokemon_id = get_id_from_pokemon_dict(
            pokemon_dict=pokemon_dict
        )
        try:
            pokemon = PokemonImageCache.objects.get(pokemon_id=pokemon_id)
            return pokemon.url_image
        except ObjectDoesNotExist:
            details = self.get_pokemon_details(pokemon_id=pokemon_id)
            for url in details['sprites'].values():
                if url is not None:
                    PokemonImageCache.objects.create(
                        pokemon_id=pokemon_id,
                        url_image=url
                    )
                    return url
        return None

    def get_pokemon_details(
            self,
            pokemon_id: str
    ) -> Dict:
        data = self.connector.get(
            resource=f'pokemon/{pokemon_id}/'
        )
        return data