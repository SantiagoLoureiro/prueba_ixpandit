# local imports
from pokeapp.controllers import PokemonApiController

# Python imports
from typing import List


def search_pokemons(
        name: str
) -> List:
    name = name.lower()
    pokemons_list = PokemonApiController().get_all_pokemons()
    pokemons_filter = list(
        filter(lambda pokemon: name in pokemon['name'], pokemons_list)
    )
    load_pokemons_images(pokemons_list=pokemons_filter)
    return pokemons_filter


def load_pokemons_images(
        pokemons_list: List
) -> List:
    controller = PokemonApiController()
    for pokemon in pokemons_list:
        pokemon['image_url'] = controller.get_pokemon_image_url(
            pokemon_dict=pokemon
        )
    return pokemons_list
