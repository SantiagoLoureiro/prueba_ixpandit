# Python imports
import requests
from typing import Dict


class Connector(object):
    base_url = ''

    def __init__(
            self,
            base_url: str
    ):
        self.base_url = base_url

    def get(
        self,
        resource: str
    ) -> Dict:
        response = requests.get(
            self.base_url + resource
        )
        data = response.json()
        return data


def get_id_from_pokemon_dict(
        pokemon_dict: Dict
) -> str:
    url = pokemon_dict['url']
    list_url = url.split('/')
    return list_url[-2]
