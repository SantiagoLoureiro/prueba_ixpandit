# Django imports
from django.test import TestCase

# Local imports
from pokeapp import services
from pokeapp.models import PokemonImageCache


class PokeappTestCase(TestCase):
    def setUp(self):
        PokemonImageCache.objects.create(pokemon_id="pikachu", url_image="test")

    def test_search_pokemons(self):
        name = 'pikachu'
        pokemons_list = services.search_pokemons(
            name=name
        )
        list_name = [d['name'] for d in pokemons_list]
        self.assertIn(name, list_name)

    def test_parcial_name_search_pokemons(self):
        parcial_name = 'pikach'
        complete_name = 'pikachu'
        pokemons_list = services.search_pokemons(
            name=parcial_name
        )
        list_name = [d['name'] for d in pokemons_list]
        self.assertIn(complete_name, list_name)

    def test_load_pokemons_images_from_cache(self):
        pokemons_list = [
            {'name': 'pikachu', 'url': 'https://pokeapi.co/api/v2/pokemon/25/'}
        ]
        services.load_pokemons_images(
            pokemons_list=pokemons_list
        )
        item = pokemons_list.pop()
        self.assertIn('image_url', item.keys())

    def test_load_pokemons_images(self):
        pokemons_list = [
            {'name': 'no_cache_pokemon', 'url': 'https://pokeapi.co/api/v2/pokemon/25/'}
        ]
        services.load_pokemons_images(
            pokemons_list=pokemons_list
        )
        item = pokemons_list.pop()
        self.assertIn('image_url', item.keys())



